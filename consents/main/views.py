import datetime

from django.views.generic import TemplateView


class ConsentView(TemplateView):
    template_name = 'consent_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['today'] = datetime.date.today()
        return context
