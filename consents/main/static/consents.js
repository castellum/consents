var canvas = document.querySelector('[data-js="signature"]');
var canvasReset = document.querySelector('[data-js="signature-reset"]');
var ctx = canvas.getContext('2d');
var activePointerId = null;

canvas.style.touchAction = 'none';

var getScale = function() {
    var rect = canvas.getBoundingClientRect();
    return canvas.width / rect.width;
}

document.addEventListener('click', event => {
    if (event.target.matches('[data-js="signature-reset"]')) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
})

canvas.addEventListener('pointerdown', event => {
    if (activePointerId === null && (event.buttons & 1 || event.pointerType !== 'mouse')) {
        var scale = getScale();
        canvas.setPointerCapture(event.pointerId);
        activePointerId = event.pointerId;
        ctx.moveTo(event.offsetX * scale, event.offsetY * scale);
        ctx.beginPath();
    }
});
canvas.addEventListener('pointermove', event => {
    if (event.pointerId === activePointerId) {
        var scale = getScale();
        ctx.lineTo(event.offsetX * scale, event.offsetY * scale);
        ctx.stroke();
    }
});
canvas.addEventListener('pointerup', event => {
    if (event.pointerId === activePointerId) {
        var scale = getScale();
        ctx.lineTo(event.offsetX * scale, event.offsetY * scale);
        ctx.stroke();
        activePointerId = null;
    }
});

var printForm = document.querySelector('[data-js="print"]');

printForm.addEventListener('submit', event => {
    event.preventDefault();
    window.print();
});
