from bootstrap_colors.views import BootstrapColorsView
from django.contrib import admin
from django.urls import path

from consents.main.views import ConsentView

urlpatterns = [
    path('', ConsentView.as_view()),
    path('admin/', admin.site.urls),
    path('colors.css', BootstrapColorsView.as_view(), name='colors')
]
