This is prototype for a consent tool for internal use at MPIB.

# Quickstart

Run `make` to start a development setup. python and npm are required.

For members of MPIB, a demo server is available at
<https://consents.t.mpib-berlin.mpg.de>.
