FROM buildpack-deps:bullseye AS base

ENV PYTHONUNBUFFERED 1
ENV PKGS gettext python3 python3-pip python3-wheel uwsgi uwsgi-plugin-python3 python3-psycopg2 python3-ldap libldap-common libsasl2-modules
ENV DJANGO_SETTINGS_MODULE=django_settings

RUN useradd uwsgi

EXPOSE 8000/tcp

RUN apt-get update -q && apt-get install -y --no-install-recommends $PKGS && rm -rf /var/lib/apt/lists/*

WORKDIR /app/
COPY setup.cfg setup.py package.json uwsgi.ini LICENSE ./

RUN pip3 install -e .

COPY consents/ consents

CMD ["uwsgi", "uwsgi.ini"]


FROM base AS static
RUN apt-get update -q && apt-get install -y --no-install-recommends npm
RUN npm install --omit=dev
RUN django-admin collectstatic --no-input --settings=consents.settings.bare


FROM base AS final
COPY --from=static /app/static/ static
